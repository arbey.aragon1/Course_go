package servidor

import (
	"io"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hola Servidor Go!")
}

func Run() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/ii", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Route: ii!")
	})
	http.ListenAndServe(":8000", nil)
}
