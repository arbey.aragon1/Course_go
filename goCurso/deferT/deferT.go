package deferT

import "fmt"

func defetTest() {
	fmt.Println("final")
}

func TestDefer() {
	defer defetTest()
	fmt.Println("corrida")
}
