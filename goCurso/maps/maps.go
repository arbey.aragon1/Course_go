package maps

func GetMap(name string) int {
	//mapTest := make(map[string]int)
	mapTest := map[string]int{
		"juan": 18,
		"jose": 5,
	}
	mapTest["key1"] = 1
	mapTest["key2"] = 2
	delete(mapTest, "key1")
	delete(mapTest, "jose")
	return mapTest[name]
}
