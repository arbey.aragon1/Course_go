package main
 
import "fmt"
 
type Human struct{
	name string
}


type Dummy struct{
	name string
}


type Tutor struct{
	Human
	Dummy
}

func (this *Tutor) hablar() string{
	return "hola"
}

func main() {
	tutor := Tutor{Human{"Uriel"},Dummy{"Uriel2"}}

    fmt.Println(tutor.Human.name)
}