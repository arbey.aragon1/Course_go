package main
 
import "fmt"
 
type User interface {
	Permisos() int
	Name() string
}

type Admin struct{
	name string
}

func (this Admin) Permisos() int{
	return 5
}

func (this Admin) Name() string{
	return this.name
}



type Editor struct{
	name string
}

func (this Editor) Permisos() int{
	return 3
}

func (this Editor) Name() string{
	return this.name
}


func auth(user User) string{
	if user.Permisos() >= 4 {
		return "ad"
	}
	return "ed"
}

func main() {
	//admin := Admin{"ususrio"}
	//editor := Editor{"ususrio"}

	//fmt.Println(auth(admin))
	//fmt.Println(auth(editor))

	
	usuarios := []User{Admin{"ususrio"},Editor{"ususrio"}}
	for _,usuario := range usuarios{
		fmt.Println(auth(usuario))
	}

}