package main
 
import "fmt"
 
func main() {
	slice := make([]int,3,5)

	slice = append(slice, 2)
	slice = append(slice, 2)
	slice = append(slice, 2)
	slice = append(slice, 2)
	slice = append(slice, 2)
	slice = append(slice, 2)
	slice = append(slice, 2)
    fmt.Println(slice)
	fmt.Println(cap(slice))

	/*
	copia el minimo de elementos
	*/
	slice2 := []int{1,2,3,4}
	copia := make([]int,len(slice2))

	copy(copia,slice2)
	
	fmt.Println(slice2)
	fmt.Println(copia)

}