package main
 
import "fmt"
 
type User struct{
	edad int
	nombre,apellido string
}

func (this User) nombre_completo() string{
	return this.nombre + " " + this.apellido
}

func (this *User) set_name(n string){
	this.nombre = n
}

func main() {
	url := new(User)
	url.nombre = "Arbey"
	url.apellido = "Aragon"

	url.set_name("Aragon")

	fmt.Println(url.nombre_completo())
}