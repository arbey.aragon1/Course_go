package main
 
import(
	"fmt"
	"bufio"
	"os"
)
 
func main() {
	//fmt.Println(string(file_data))
	leeArchivo()
}

func leeArchivo() bool{
	archivo, error := os.Open("./holaa.txt")
	defer func(){
		archivo.Close()
		fmt.Println("Defer")
	}()


	if error != nil{
		panic("Error!")
	}

	scanner := bufio.NewScanner(archivo)

	for scanner.Scan(){
		line := scanner.Text()
		fmt.Println(line)
	}

	if true{
		return true
	}

	return false
}