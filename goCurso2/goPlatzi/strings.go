package main

import (
	"fmt"
	"strings"
)

func main() {
	stringLibrary()
}

func stringLibrary() {
	text := "Hello world. Hello Platzi. Hello Go"
	// To uppercase
	fmt.Println(strings.ToUpper(text))
	// To lowercase
	fmt.Println(strings.ToLower(text))
	// Replace(string-to-modified, old-string, new-string, times-to-replace)
	fmt.Println(strings.Replace(text, "Hello", "Hi", -1))
	// string => slice
	fmt.Println(strings.Split(text, "."))
}
