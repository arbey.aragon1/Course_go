package main

import "fmt"

func main() {
	array()
	slice()
}

func array() {
	// vat name[size] type
	var array [3]string
	fmt.Println(array)    // [ ]
	fmt.Println(array[0]) // 'Nothing'
	// Initialization
	array[0] = "First"
	array[1] = "Second"
	array[2] = "Thrid"
	fmt.Println(array)
	fmt.Println(array[0]) // First

	array[2] = "Last"
	fmt.Println(array) // [First Second Last]
}

func slice() {
	// var name []type
	var slice []string
	fmt.Println(slice) // []
	// Initialization
	slice = append(slice, "First")
	fmt.Println(slice[0]) // First
	slice = append(slice, "Second")
	fmt.Println(slice) // [First Second]

	slice[0] = "One"
	fmt.Println(slice) // [One Second]
}
