package main

import "fmt"

func main() {
	threeFors()
}

func threeFors() {
	// 1. Normal version
	fmt.Println("For, normal version")
	for i := 0; i < 100; i += 10 {
		fmt.Println(i)
	}
	// 2. While version
	fmt.Println("For, while version")
	i := 100
	for i > 0 {
		i -= 10
		fmt.Println(i)
	}
	// 3. Infinite version
	fmt.Println("For, infinite version")
	j := 0
	for {
		j += 1
		fmt.Println(j)
		if j == 10 {
			fmt.Println("The end!")
			break
		}
	}
}
